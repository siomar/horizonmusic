import styled from 'styled-components/native';

export const Container = styled.View``;

export const HeaderBox = styled.View`
  width: 100%;
  padding: 70px 24px 20px;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const Logo = styled.Text`
  font-weight: 800;
  font-size: 20px;
`;

export const Title = styled.Text`
  color: #fff;
  font-size: 24px;
`;
