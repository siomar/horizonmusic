import React from 'react';
import {View, Image, Dimensions, ImageBackground} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Feather';

import {Container, HeaderBox, Logo, Title} from './styles';

const SCREEN_WIDTH = Dimensions.get('window').width;

interface dataVideo {
  capa?: string;
  title?: string;
  tamanho: number;
}

const Card: React.FC<dataVideo> = ({capa, title, tamanho}: dataVideo) => {
  return (
    <ImageBackground
      source={{
        uri: capa,
      }}
      imageStyle={{
        resizeMode: 'cover',
      }}
      style={{
        backgroundColor: '#333',
        width: SCREEN_WIDTH,
        height: tamanho,
        position: 'relative',
      }}>
      <LinearGradient
        colors={['rgba(255, 65, 108,0.75)', 'rgba(255, 75, 43,0.75)']}
        style={{flex: 1}}>
        <View
          style={{
            padding: 24,
            position: 'absolute',
            bottom: 12,
          }}>
          <Image
            source={{
              uri:
                'https://yt3.ggpht.com/ytc/AAUvwnghXj_FjRZ9OHRbE2hRm236dQeXjJqr0dqb_WOnd7s=s176-c-k-c0x00ffffff-no-rj-mo',
            }}
            resizeMode="cover"
            style={{width: 40, height: 40, borderRadius: 10, marginBottom: 12}}
          />
          <Title>{title}</Title>
        </View>
      </LinearGradient>
    </ImageBackground>
  );
};

export default Card;
