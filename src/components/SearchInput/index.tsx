import React, {useCallback, useRef, useState} from 'react';
import {TextInputProps, View} from 'react-native';
import {Container, TextInput, Icon} from './styles';

interface InputProps extends TextInputProps {
  value?: string;
}

const SearchInput: React.FC<InputProps> = ({value, ...rest}) => {
  const inputElementRef = useRef<any>(null);
  const [isFocused, setIsFocused] = useState(false);
  const [isFilled, setIsFilled] = useState(false);

  const handleInputFocus = useCallback(() => {
    setIsFocused(true);
  }, []);

  const handleInputBlur = useCallback(() => {
    setIsFocused(false);

    setIsFilled(!!value);
  }, [value]);

  return (
    <View>
      <TextInput
        ref={inputElementRef}
        keyboardAppearance="dark"
        placeholderTextColor="#B7B7CC"
        defaultValue={value}
        onFocus={handleInputFocus}
        onBlur={handleInputBlur}
        testID="search-input"
        {...rest}
      />
      <Icon
        name="search"
        size={20}
        color={isFocused || isFilled ? '#C72828' : '#B7B7CC'}
      />
    </View>
  );
};

export default SearchInput;
