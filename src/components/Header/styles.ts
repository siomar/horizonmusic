import {useRoute} from '@react-navigation/native';
import styled, {css} from 'styled-components/native';
// const route = useRoute();

interface TabProps {
  isActive: boolean;
}

export const HeaderBox = styled.View`
  width: 100%;
  padding: 60px 24px 20px;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const Content = styled.View`
  width: 100%;
  padding: 0px 24px 20px;

  flex-direction: row;
`;

export const TitlePage = styled.Text<TabProps>`
  margin-top: 20px;
  font-size: 17px;
  font-family: 'Montserrat-Medium';
  font-weight: 800;
  margin-right: 20px;
  color: #555;

  ${(props) =>
    props.isActive &&
    css`
      color: #000;
      margin-top: 10px;
      font-size: 30px;
    `};
`;
