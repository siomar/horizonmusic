import React, {useCallback, useEffect, useState} from 'react';
import {View} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {navigate, getNavigate} from '../../routes/RootNavigation';

import {HeaderBox, Content, TitlePage} from './styles';
import {TouchableOpacity} from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';

const Header: React.FC = () => {
  const [selected, setSelected] = useState<'Home' | 'Search' | 'Playlist'>(
    'Home',
  );

  // useEffect(() => {
  //   navigate(selected, {});
  // }, []);

  const handleNavigate = useCallback(
    (route) => {
      setSelected(route);
      navigate(route, {});
    },
    [selected],
  );
  return (
    <View style={{backgroundColor: '#fff'}}>
      <HeaderBox>
        <View
          style={{
            flex: 1,
          }}>
          <Icon size={35} name="codesandbox" color="#000" />
        </View>
      </HeaderBox>
      <Content>
        <TouchableOpacity onPress={() => handleNavigate('Home')}>
          <TitlePage isActive={selected === 'Home'}>Home</TitlePage>
          {selected === 'Home' && (
            <LinearGradient
              colors={['#00C9FF', '#92FE9D']}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              style={{
                width: '50%',
                height: 4,
                marginTop: 10,
              }}></LinearGradient>
          )}
        </TouchableOpacity>
        <TouchableOpacity onPress={() => handleNavigate('Search')}>
          <TitlePage isActive={selected === 'Search'}>Search</TitlePage>
          {selected === 'Search' && (
            <LinearGradient
              colors={['#00C9FF', '#92FE9D']}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              style={{
                width: '50%',
                height: 4,
                marginTop: 10,
              }}></LinearGradient>
          )}
        </TouchableOpacity>
        <TouchableOpacity onPress={() => handleNavigate('Playlist')}>
          <TitlePage isActive={selected === 'Playlist'}>Playlist</TitlePage>
          {selected === 'Playlist' && (
            <LinearGradient
              colors={['#00C9FF', '#92FE9D']}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              style={{
                width: '50%',
                height: 4,
                marginTop: 10,
              }}></LinearGradient>
          )}
        </TouchableOpacity>
      </Content>
    </View>
  );
};

export default Header;
