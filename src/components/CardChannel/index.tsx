import React from 'react';
import {
  View,
  Image,
  Dimensions,
  ImageBackground,
  GestureResponderEvent,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Feather';

import {Container, HeaderBox, Logo, Title} from './styles';

interface dataVideo {
  capa?: string;
  title?: string;
}
// onPress: (event: GestureResponderEvent) => void;

const CardChannel: React.FC<dataVideo> = ({capa, title, ...rest}) => {
  return (
    <Container {...rest}>
      <Image
        source={{
          uri: capa,
        }}
        resizeMode="cover"
        style={{
          width: 40,
          height: 40,
          borderRadius: 10,
        }}
      />
      <View
        style={{
          marginLeft: 10,
          flex: 1,
        }}>
        <Title>{title}</Title>
      </View>
    </Container>
  );
};

export default CardChannel;
