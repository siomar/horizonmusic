import styled from 'styled-components/native';

export const Container = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  margin-left: 10px;
  padding: 10px 14px;
  border-radius: 10px;
  /* background-color: #333; */
`;

export const HeaderBox = styled.View`
  width: 100%;
  padding: 70px 24px 20px;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const Logo = styled.Text`
  font-weight: 800;
  font-size: 20px;
`;

export const Title = styled.Text`
  color: #fff;
  font-size: 18px;
  font-weight: 500;
`;
