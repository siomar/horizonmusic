import styled from 'styled-components/native';
import {FlatList} from 'react-native';

export const PlayerControl = styled.View`
  padding: 10px 24px;
  width: 100%;
  flex-direction: row;
  align-items: center;
  align-content: center;
  justify-content: space-around;
`;

export const PlayerControlBox = styled.View`
  padding: 15px;
  width: 100%;
  flex-direction: row;
  align-items: center;
  align-content: center;
  justify-content: space-around;
  border-radius: 20px;
`;

export const MusicList = styled(FlatList as new () => FlatList)`
  width: 100%;
`;

export const Music = styled.TouchableOpacity`
  width: 100%;
  padding: 5px 24px 0px;
`;

export const MusicConteiner = styled.TouchableOpacity`
  flex: 1;
  padding: 20px 0px;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const MusicTitle = styled.Text`
  color: #fff;
  margin-bottom: 7px;
  font-size: 18px;
`;

export const MusicAutor = styled.Text`
  font-size: 14px;
  color: #ededed;
`;

export const Title = styled.Text`
  font-size: 20px;
  font-weight: 700;
  color: #fff;
  align-items: center;
`;
