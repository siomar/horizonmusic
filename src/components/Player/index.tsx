import React, {useCallback, useEffect, useRef, useState} from 'react';
import {View, Dimensions, Text} from 'react-native';
import Video from 'react-native-video';
import Animated, {
  Extrapolate,
  interpolate,
  useAnimatedStyle,
} from 'react-native-reanimated';
import {
  PlayerControl,
  PlayerControlBox,
  MusicList,
  Music,
  MusicTitle,
  MusicAutor,
  Title,
  MusicConteiner,
} from './styles';
import Icon from 'react-native-vector-icons/Feather';

import {TouchableOpacity} from 'react-native-gesture-handler';
import {useSongs} from '../../hooks/songs';
import LinearGradient from 'react-native-linear-gradient';

const SCREEN_WIDTH = Dimensions.get('window').width;

interface IboxPosition {
  value: number;
}
interface propsData {
  boxPosition: IboxPosition;
}

interface IVideoData {
  path: string;
  id: string;
  title: string;
  channel: string;
  channelId: string;
}

const Player: React.FC<propsData> = ({boxPosition}) => {
  const videoEl = useRef('videoPlayer');
  const {data, getMusics} = useSongs();
  const [songs, setSongs] = useState<IVideoData[]>([] as IVideoData);
  const [songPlay, setSongPlay] = useState(0);

  const [paused, setPaused] = useState(false);
  const [muted, setMuted] = useState(false);

  useEffect(() => {
    async function getVideo() {
      const videos = await getMusics<IVideoData[]>();
      await setSongs(videos);
    }
    getVideo();
  }, [data, getMusics]);

  const playToMusic = useCallback((index: number) => {
    setSongPlay(index);
  }, []);

  const handlePlayerVideo = useCallback(() => setPaused((item) => !item), [
    paused,
  ]);
  const handlePlayerVideoMuted = useCallback(() => setMuted((item) => !item), [
    paused,
  ]);
  const beforeMusic = useCallback(() => {
    const index = songPlay;
    if (0 < index) {
      setSongPlay((item) => index - 1);
    }
  }, [songPlay]);
  const afterMusic = useCallback(() => {
    const index = songPlay;
    if (songs.length - 1 > index) {
      setSongPlay((item) => index + 1);
    }
  }, [songPlay]);

  const videoStyle = useAnimatedStyle(() => {
    return {
      width: interpolate(
        boxPosition.value,
        [0, 696],
        [SCREEN_WIDTH, 140],
        Extrapolate.CLAMP,
      ),
    };
  });
  const videoBoxStyle = useAnimatedStyle(() => {
    return {
      height: interpolate(
        boxPosition.value,
        [0, 696],
        [280, 90],
        Extrapolate.CLAMP,
      ),
      marginTop: interpolate(
        boxPosition.value,
        [0, 696],
        [120, 0],
        Extrapolate.CLAMP,
      ),
    };
  });
  const opacityBtns = useAnimatedStyle(() => {
    return {
      opacity: interpolate(
        boxPosition.value,
        [0, 348, 696],
        [0, 0, 1],
        Extrapolate.CLAMP,
      ),
    };
  });
  const opacityClose = useAnimatedStyle(() => {
    return {
      opacity: interpolate(
        boxPosition.value,
        [0, 348, 696],
        [1, 0, 0],
        Extrapolate.CLAMP,
      ),
    };
  });
  const opacityTitle = useAnimatedStyle(() => {
    return {
      opacity: interpolate(
        boxPosition.value,
        [0, 348, 696],
        [1, 0, 0],
        Extrapolate.CLAMP,
      ),
    };
  });
  return (
    <LinearGradient
      start={{x: 0, y: 1}}
      end={{x: 0, y: 0}}
      colors={['rgba(0, 0, 0,0.95)', 'rgba(0, 0, 0,0.95)']}
      style={{
        flex: 3,
        flexDirection: 'column',
        alignItems: 'flex-start',
        position: 'relative',
      }}>
      <Animated.View
        style={[
          {
            paddingTop: 40,
            width: SCREEN_WIDTH,
            position: 'absolute',
            left: 0,
            right: 0,
            alignItems: 'center',
          },
          opacityClose,
        ]}>
        <Icon name="chevron-down" size={35} color={'#ddd'} />
      </Animated.View>
      <Animated.View style={[videoBoxStyle]}>
        <Animated.View
          style={[
            {
              flex: 1,
            },
            videoStyle,
          ]}>
          <Video
            ref={videoEl}
            source={{
              uri: songs[songPlay].path,
            }}
            resizeMode={'cover'}
            style={{flex: 1, backgroundColor: '#000'}}
            paused={paused}
            muted={muted}
            pictureInPicture={true}
            playInBackground={true}
            poster={'https://baconmockup.com/300/200/'}
          />
        </Animated.View>
      </Animated.View>

      <Animated.View
        style={[
          opacityBtns,
          {
            position: 'absolute',
            right: 0,
            padding: 15,
            flexDirection: 'row',
            justifyContent: 'space-around',
            alignItems: 'flex-start',
            width: '65%',
          },
        ]}>
        <Title style={{fontSize: 14, flex: 1}}>
          {songs[songPlay] && songs[songPlay].title}
        </Title>
        <TouchableOpacity
          onPress={handlePlayerVideo}
          style={{flex: 2, padding: 10}}>
          <Icon name={paused ? 'play' : 'pause'} size={30} color={'#ddd'} />
        </TouchableOpacity>
      </Animated.View>
      {songs[songPlay] && (
        <Animated.View style={[{padding: 20}, opacityTitle]}>
          <Title>{songs[songPlay].title}</Title>
          <MusicAutor>{songs[songPlay].channel}</MusicAutor>
        </Animated.View>
      )}
      <MusicList
        data={songs}
        ListHeaderComponent={() => (
          <PlayerControl>
            <PlayerControlBox>
              <TouchableOpacity>
                <Icon name="list" size={24} color={'#ddd'} />
              </TouchableOpacity>
              <TouchableOpacity onPress={beforeMusic}>
                <Icon name="skip-back" size={30} color={'#ddd'} />
              </TouchableOpacity>
              <TouchableOpacity onPress={handlePlayerVideo}>
                <Icon
                  name={paused ? 'play' : 'pause'}
                  size={40}
                  color={'#ddd'}
                />
              </TouchableOpacity>
              <TouchableOpacity onPress={afterMusic}>
                <Icon name="skip-forward" size={30} color={'#ddd'} />
              </TouchableOpacity>
              <TouchableOpacity onPress={handlePlayerVideoMuted}>
                <Icon
                  name={muted ? 'volume-x' : 'volume-2'}
                  size={24}
                  color={'#ddd'}
                />
              </TouchableOpacity>
            </PlayerControlBox>
          </PlayerControl>
        )}
        renderItem={({item, index}) => (
          <TouchableOpacity
            key={index}
            onPress={() => playToMusic(index)}
            style={{width: '100%', paddingHorizontal: 24}}>
            <MusicConteiner style={{borderBottomColor: '#ededed'}}>
              <View
                style={{
                  flex: 8,
                  flexDirection: 'column',
                  justifyContent: 'flex-start',
                }}>
                <MusicTitle>{item.title}</MusicTitle>
                <MusicAutor>{item.channel}</MusicAutor>
              </View>
            </MusicConteiner>
          </TouchableOpacity>
        )}
      />
    </LinearGradient>
  );
};

export default Player;
