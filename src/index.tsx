import 'react-native-gesture-handler';
import React from 'react';
import {View, StatusBar, LogBox} from 'react-native';

// import { Container } from './styles';

import AppProvider from './hooks';
import Routes from './routes';

const App: React.FC = () => {
  LogBox.ignoreLogs(['Warning: ...']);

  return (
    <View
      style={{
        flex: 1,
      }}>
      <StatusBar
        barStyle="dark-content"
        backgroundColor="transparent"
        translucent
      />
      <AppProvider>
        <Routes />
      </AppProvider>
    </View>
  );
};

export default App;
