import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();

import Tabs from './Tabs';
import {navigationRef} from './RootNavigation';
import Channel from '../pages/Channel';
import Music from '../pages/Music';

const Routes: React.FC = () => {
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator>
        <Stack.Screen
          options={{
            headerShown: false,
            gestureEnabled: false,
          }}
          name="Tabs"
          component={Tabs}
        />
        <Stack.Screen
          options={{
            headerShown: false,
            gestureEnabled: false,
          }}
          name="Channel"
          component={Channel}
        />
        <Stack.Screen
          options={{
            headerShown: false,
            gestureEnabled: false,
          }}
          name="Music"
          component={Music}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Routes;
