import React, {useEffect, useState} from 'react';
import {View, StyleSheet, Dimensions, Text} from 'react-native';
import Animated, {
  useSharedValue,
  useAnimatedStyle,
  useAnimatedGestureHandler,
  interpolate,
  Extrapolate,
} from 'react-native-reanimated';
import {PanGestureHandler} from 'react-native-gesture-handler';
import Player from '../../components/Player';
import Pages from './pages.routes';
import Header from '../../components/Header';
import {useSongs} from '../../hooks/songs';

const SCREEN_HEIGHT = Dimensions.get('window').height;

const Tabs: React.FC = () => {
  const {data} = useSongs();
  const boxPosition = useSharedValue(SCREEN_HEIGHT - 80);

  const boxStyle = useAnimatedStyle(() => {
    return {
      transform: [
        {translateY: data.length > 0 ? boxPosition.value : SCREEN_HEIGHT},
      ],
    };
  });

  const onGestireEvent = useAnimatedGestureHandler({
    onStart(event: any, ctx: any) {
      ctx.boxPosition = boxPosition.value;
    },
    onActive(event: any, ctx: any) {
      if (
        ctx.boxPosition + event.translationY < 23 ||
        ctx.boxPosition + event.translationY > SCREEN_HEIGHT - 80
      ) {
        return null;
      }
      boxPosition.value = ctx.boxPosition + event.translationY;
    },
    onEnd(event: any, ctx: any) {
      if (ctx.boxPosition + event.translationY > SCREEN_HEIGHT / 2) {
        boxPosition.value = SCREEN_HEIGHT - 80;
      } else {
        boxPosition.value = 0;
      }
    },
  });

  return (
    <View style={styles.container}>
      <Header />
      <View style={{flex: 1}}>
        <Pages />
      </View>
      <PanGestureHandler onGestureEvent={onGestireEvent}>
        <Animated.View style={[styles.box, boxStyle]}>
          <View
            style={{
              marginBottom: 65,
              paddingHorizontal: 24,
            }}>
            <View
              style={{
                backgroundColor: '#303030',
              }}>
              <Text>Ola mundio</Text>
            </View>
          </View>

          <Player boxPosition={boxPosition} />
        </Animated.View>
      </PanGestureHandler>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  box: {
    backgroundColor: 'transparent',
    height: SCREEN_HEIGHT,
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
  },
});

export default Tabs;
