import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();

import Home from '../../pages/Home';
import Playlist from '../../pages/Playlist';
import Search from '../../pages/Search';

import {navigationRef, getRoute} from '../RootNavigation';
import Intro from '../../pages/Intro';

const Pages: React.FC = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        options={{
          headerShown: false,
          gestureEnabled: false,
        }}
        name="Home"
        component={Home}
      />
      <Stack.Screen
        options={{
          headerShown: false,
          gestureEnabled: false,
        }}
        name="Search"
        component={Search}
      />
      <Stack.Screen
        options={{
          headerShown: false,
          gestureEnabled: false,
        }}
        name="Playlist"
        component={Playlist}
      />
    </Stack.Navigator>
  );
};

export default Pages;
