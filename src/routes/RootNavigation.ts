import {createRef} from 'react';

export const navigationRef = createRef();

export function navigate(name, params) {
  navigationRef.current?.navigate(name, params);
}

export function goBack() {
  navigationRef.current?.goBack();
}

export function getNavigate() {
  console.log(navigationRef.current);
}

export function getRoute(nav) {
  console.log(nav);
}
