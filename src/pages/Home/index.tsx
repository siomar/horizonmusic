import React, {useCallback, useEffect, useState} from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
  Text,
  ImageBackground,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {MusicList, Title, TitleList} from './styles';

import categorias from '../../assets/generos.json';
import {ScrollView} from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';
import {useNavigation} from '@react-navigation/native';
import api from '../../services/api';

const Home: React.FC = () => {
  const [genre, setGenre] = useState<number>(0);
  const [channels, setChannels] = useState([]);
  const [songs, setSongs] = useState([]);
  const [songsRelevance, setSongsRelevance] = useState([]);

  const navigation = useNavigation();

  const handleVideoDetails = useCallback(
    (item) => {
      navigation.navigate('Music', {videoId: item.id});
    },
    [navigation],
  );

  const handleChannelDetails = useCallback(
    (item) => {
      navigation.navigate('Channel', {channelId: item.id});
    },
    [navigation],
  );

  const getGenreTitle = useCallback(
    (item) => {
      return categorias.find((item) => item.id === genre).title;
    },
    [genre],
  );

  useEffect(() => {
    async function getChannels() {
      try {
        setChannels([]);
        const {topicId} = categorias.find((item) => item.id === genre);
        const response = await api.get(
          `https://www.googleapis.com/youtube/v3/search?key=AIzaSyDn8cOcNwm6JJWK78KVbaRk663ihWOYqc4&part=snippet&maxResults=50&type=channel&order=viewCount&topicId=${topicId}`,
        );
        setChannels(response.data.items);
      } catch (error) {
        console.log(error);
      }
    }

    async function getSongs() {
      try {
        setSongs([]);
        const {topicId} = categorias.find((item) => item.id === genre);
        const response = await api.get(
          `https://www.googleapis.com/youtube/v3/search?key=AIzaSyDn8cOcNwm6JJWK78KVbaRk663ihWOYqc4&part=snippet&maxResults=50&type=video&order=viewCount&topicId=${topicId}`,
        );
        setSongs(response.data.items);
      } catch (error) {
        console.log(error);
      }
    }
    async function getSongsRelevance() {
      try {
        setSongsRelevance([]);
        const {topicId} = categorias.find((item) => item.id === genre);
        const response = await api.get(
          `https://www.googleapis.com/youtube/v3/search?key=AIzaSyDn8cOcNwm6JJWK78KVbaRk663ihWOYqc4&part=snippet&maxResults=50&type=video&order=relevance&topicId=${topicId}`,
        );
        setSongsRelevance(response.data.items);
      } catch (error) {
        console.log(error);
      }
    }

    getChannels();
    getSongs();
    getSongsRelevance();
  }, [genre]);

  return (
    <View style={{flex: 1, position: 'relative', backgroundColor: '#fff'}}>
      <ScrollView>
        {/* <View style={{paddingHorizontal: 24}}>
          <TitleList>Histórico</TitleList>
        </View>
        <MusicList
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          data={videos.items}
          keyExtractor={(item) => item.id.videoId}
          renderItem={(item, index) => (
            <ImageBackground
              imageStyle={{borderRadius: 8}}
              source={{uri: item.item.snippet.thumbnails.medium.url}}
              resizeMode="cover"
              style={{
                width: 60,
                height: 60,
                marginRight: 15,
                marginBottom: 25,
              }}></ImageBackground>
          )}
        /> */}
        <View style={{paddingHorizontal: 24}}>
          <TitleList>Gêneros musicais</TitleList>
        </View>
        <MusicList
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          data={categorias}
          keyExtractor={(item) => item.id}
          renderItem={(item, index) => (
            <TouchableOpacity onPress={() => setGenre(item.item.id)}>
              <LinearGradient
                colors={
                  genre === item.item.id ? item.item.colors : ['#bbb', '#777']
                }
                start={{x: 0, y: 1}}
                end={{x: 1, y: 0}}
                style={{
                  width: 100,
                  height: 100,
                  marginRight: 15,
                  position: 'relative',
                  marginBottom: 25,
                  borderRadius: 18,
                }}>
                <View
                  style={{
                    flex: 1,
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="music"
                    size={25}
                    color={'#fff'}
                    style={{margin: 5}}
                  />
                  <Title style={{color: '#fff'}}>{item.item.title}</Title>
                </View>
              </LinearGradient>
            </TouchableOpacity>
          )}
        />
        {channels.length !== 0 && (
          <>
            <View style={{paddingHorizontal: 24}}>
              <TitleList>Canais de {getGenreTitle<string>()}</TitleList>
            </View>
            <MusicList
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              data={channels}
              keyExtractor={(item) => {
                return item.id.channelId;
              }}
              renderItem={(item, index) => (
                <TouchableOpacity
                  onPress={() =>
                    handleChannelDetails({id: item.item.id.channelId})
                  }>
                  <ImageBackground
                    imageStyle={{borderRadius: 15}}
                    source={{uri: item.item.snippet.thumbnails.medium.url}}
                    resizeMode="cover"
                    style={{
                      width: 90,
                      height: 90,
                      marginRight: 15,
                      position: 'relative',
                      marginBottom: 25,
                    }}>
                    <View
                      style={{
                        opacity: item.item.id.channelId ? 0 : 1,
                        flex: 1,
                        padding: 10,
                        position: 'absolute',
                        bottom: 0,
                        backgroundColor: 'rgba(0,0,0,0.8)',
                        borderBottomLeftRadius: 8,
                      }}>
                      <Title>{item.item.snippet.title}</Title>
                    </View>
                  </ImageBackground>
                </TouchableOpacity>
              )}
            />
          </>
        )}

        <View style={{paddingHorizontal: 24}}>
          <TitleList>Mais vistos</TitleList>
        </View>
        <MusicList
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          data={songs}
          keyExtractor={(item) => item.id.videoId}
          renderItem={(item, index) => (
            <TouchableOpacity
              onPress={() => handleVideoDetails({id: item.item.id.videoId})}>
              <ImageBackground
                imageStyle={{borderRadius: 8}}
                source={{uri: item.item.snippet.thumbnails.medium.url}}
                resizeMode="cover"
                style={{
                  width: 170,
                  height: 180,
                  marginRight: 15,
                  position: 'relative',
                  marginBottom: 25,
                }}>
                <View
                  style={{
                    flex: 1,
                    padding: 10,
                    position: 'absolute',
                    bottom: 0,
                    backgroundColor: 'rgba(0,0,0,0.8)',
                    borderBottomLeftRadius: 8,
                  }}>
                  <Title>{item.item.snippet.title}</Title>
                </View>
              </ImageBackground>
            </TouchableOpacity>
          )}
        />
        <View style={{paddingHorizontal: 24}}>
          <TitleList>Mais relevantes</TitleList>
        </View>
        <MusicList
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          data={songsRelevance}
          keyExtractor={(item) => item.id.videoId}
          renderItem={(item, index) => (
            <TouchableOpacity
              onPress={() => handleVideoDetails({id: item.item.id.videoId})}>
              <ImageBackground
                imageStyle={{borderRadius: 8}}
                source={{uri: item.item.snippet.thumbnails.medium.url}}
                resizeMode="cover"
                style={{
                  width: 170,
                  height: 180,
                  marginRight: 15,
                  position: 'relative',
                  marginBottom: 25,
                }}>
                <View
                  style={{
                    flex: 1,
                    padding: 10,
                    position: 'absolute',
                    bottom: 0,
                    backgroundColor: 'rgba(0,0,0,0.8)',
                    borderBottomLeftRadius: 8,
                  }}>
                  <Title>{item.item.snippet.title}</Title>
                </View>
              </ImageBackground>
            </TouchableOpacity>
          )}
        />
        <MusicList
          data={songs}
          ListHeaderComponent={
            <>
              <TitleList>Musicas</TitleList>
              <LinearGradient
                colors={['#00C9FF', '#92FE9D']}
                start={{x: 0, y: 0}}
                end={{x: 1, y: 0}}
                style={{
                  width: '100%',
                  height: 4,
                  marginTop: -4,
                }}></LinearGradient>
            </>
          }
          keyExtractor={(item) => {
            if (item.id.playlistId) {
              return item.id.playlistId;
            }
            if (item.id.channelId) {
              return item.id.channelId;
            }
            return item.id.videoId;
          }}
          style={{
            width: '100%',
            paddingHorizontal: 24,
          }}
          renderItem={(item, index) => (
            <TouchableOpacity
              onPress={() => handleVideoDetails({id: item.item.id.videoId})}
              style={{
                width: '100%',
                paddingTop: 15,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <Image
                source={{uri: item.item.snippet.thumbnails.medium.url}}
                resizeMode="cover"
                style={{
                  width: 120,
                  height: 70,
                  borderRadius: 5,
                  marginRight: 15,
                }}
              />

              <View
                style={{
                  flex: 1,
                  justifyContent: 'space-between',
                }}>
                <Text style={{fontSize: 14, marginBottom: 14, flex: 1}}>
                  {item.item.snippet.title}
                </Text>
                <Text
                  style={{
                    fontSize: 12,
                    marginBottom: 14,
                    flex: 1,
                    color: '#ff5e62',
                  }}>
                  {item.item.snippet.channelTitle}
                </Text>
              </View>
            </TouchableOpacity>
          )}
        />
      </ScrollView>
    </View>
  );
};

export default Home;
