import {FlatList} from 'react-native';
import styled from 'styled-components/native';

export const Container = styled.View``;

export const Title = styled.Text`
  font-size: 12px;
  color: #fff;
  align-items: center;
  font-weight: 500;
  font-family: 'Montserrat-Bold';
  text-transform: uppercase;
`;

export const TitleList = styled.Text`
  font-family: 'Montserrat-Regular';
  font-weight: 800;
  font-size: 18px;
  color: #070707;
  align-items: center;
  margin-bottom: 20px;
`;

export const MusicList = styled(FlatList as new () => FlatList)`
  width: 100%;
  padding-left: 24px;
  margin-bottom: 50px;
  margin: 0;
`;
