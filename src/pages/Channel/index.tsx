import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  StatusBar,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {useNavigation, useRoute} from '@react-navigation/native';
import api from '../../services/api';
import Icon from 'react-native-vector-icons/Feather';

import {Container, Title, Description, HeaderBox, Button} from './styles';

// import details from '../../assets/video.json';

const SCREEN_WIDTH = Dimensions.get('window').width;

const Channel: React.FC = () => {
  const {goBack} = useNavigation();
  const {params} = useRoute();
  const [details, setDetails] = useState({});
  const [videos, setVideos] = useState([]);

  useEffect(() => {
    async function getDatails() {
      try {
        const response = await api.get(
          `https://www.googleapis.com/youtube/v3/channels?key=AIzaSyDn8cOcNwm6JJWK78KVbaRk663ihWOYqc4&part=snippet,brandingSettings&id=${params.channelId}`,
        );
        setDetails(response.data.items[0]);
      } catch (error) {
        console.log(error.message);
      }
    }

    getDatails();
  }, [params]);
  return (
    <View style={{position: 'relative', flex: 1, backgroundColor: '#fff'}}>
      <StatusBar
        barStyle="light-content"
        backgroundColor="transparent"
        translucent
      />
      <HeaderBox>
        <TouchableOpacity
          onPress={() => goBack()}
          style={{
            flex: 1,
          }}>
          <Icon size={30} name="arrow-left" color="#fff" />
        </TouchableOpacity>
      </HeaderBox>
      {details.snippet ? (
        <>
          <View
            style={{
              width: SCREEN_WIDTH,
              height: 280,
              position: 'relative',
            }}>
            <Image
              source={{uri: details.brandingSettings.image.bannerExternalUrl}}
              resizeMode="cover"
              style={{flex: 1}}
            />
            <View
              style={{
                width: SCREEN_WIDTH,
                height: 280,
                position: 'absolute',
                top: 0,
                zIndex: 8,
                backgroundColor: 'rgba(0,0,0,0.65)',
              }}></View>
          </View>
          <Image
            source={{uri: details.snippet.thumbnails.high.url}}
            style={{
              width: 100,
              height: 100,
              marginTop: -50,
              marginHorizontal: 24,
              borderRadius: 12,
            }}
            resizeMode="cover"
          />
          <ScrollView style={{flex: 1}}>
            <Container>
              <Title style={{color: '#ff5e62'}}>
                {details.snippet.channelTitle}
              </Title>
              <Title style={{marginBottom: 20}}>{details.snippet.title}</Title>
              <Description>{details.snippet.description}</Description>
            </Container>
          </ScrollView>
        </>
      ) : (
        <Title style={{color: '#ff5e62'}}>Musica não encontrada</Title>
      )}
    </View>
  );
};

export default Channel;
