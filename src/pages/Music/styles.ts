import styled from 'styled-components/native';

export const Container = styled.View`
  width: 100%;
  padding: 25px 24px 150px;
`;

export const Title = styled.Text`
  font-size: 18px;
  color: #000;
  align-items: center;
  font-weight: 500;
  font-family: 'Montserrat-Medium';
  text-transform: uppercase;
`;

export const Description = styled.Text`
  font-size: 12px;
  color: #000;
  align-items: center;
  font-weight: 500;
  font-family: 'Montserrat-Regular';
  text-transform: uppercase;
`;

export const HeaderBox = styled.View`
  position: absolute;
  z-index: 10;
  width: 100%;
  padding: 60px 24px 20px;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const Button = styled.TouchableOpacity`
  background-color: #f12711;
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 10;
  margin: 40px 24px;
  padding: 15px 35px;
  border-radius: 40px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;
