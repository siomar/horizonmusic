import React, {useCallback, useEffect, useState} from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  StatusBar,
  TouchableOpacity,
  ActivityIndicator,
  Platform,
  Alert,
} from 'react-native';
import {useNavigation, useRoute} from '@react-navigation/native';
import api from '../../services/api';
import Icon from 'react-native-vector-icons/Feather';

import YouTube from 'react-native-youtube';
import ytdl from 'react-native-ytdl';
import RNFetchBlob from 'rn-fetch-blob';

import {Container, Title, Description, HeaderBox, Button} from './styles';
import {ScrollView} from 'react-native-gesture-handler';
import {useSongs} from '../../hooks/songs';

const SCREEN_WIDTH = Dimensions.get('window').width;

interface YdtlData {
  url: string;
}

interface IGetLink {
  url: string;
  videoDetails: IVideoDetails;
}

interface IVideoDetails {
  title: string;
  ownerChannelName: string;
  channelId: string;
}

const Music: React.FC = () => {
  const {setMusics} = useSongs();
  const {goBack} = useNavigation();
  const {params} = useRoute();
  const [details, setDetails] = useState({});
  const [download, setDownload] = useState(false);

  const downloadVideo = useCallback(async () => {
    setDownload((item) => !item);
    const {url, videoDetails}: IGetLink = await getLinkVideo(params.videoId);
    await storeVideo(url, videoDetails);
  }, []);

  async function getLinkVideo(videoId: string) {
    try {
      const youtubeURL = 'https://www.youtube.com/watch?v=' + videoId;

      const {videoDetails} = await ytdl.getInfo(youtubeURL, {
        downloadURL: true,
      });
      const urls = await ytdl<YdtlData[]>(youtubeURL, {
        filter: (format) => format.container === 'mp4',
      });
      return {url: urls[0].url, videoDetails};
    } catch (error) {
      console.log(error);
      Alert.alert('Ops!', 'Não foi possivel encontrar o video');
    }
  }

  async function storeVideo(url: string, videoDetails: IVideoDetails) {
    let pathDir =
      Platform.OS === 'ios'
        ? RNFetchBlob.fs.dirs.DocumentDir
        : RNFetchBlob.fs.dirs.DownloadDir;

    let checkdir = await RNFetchBlob.fs.isDir(`${pathDir}/horizon/`);
    if (!checkdir) {
      RNFetchBlob.fs
        .mkdir(`${pathDir}/horizon`)
        .then((res) => {
          console.log('created');
        })
        .catch((error) => {
          console.log(error);
          Alert.alert('Ops!', 'Não foi possivel criar pasta para os videos');
        });
    }

    await RNFetchBlob.config({
      fileCache: true,
      path: `${pathDir}/horizon/downloadfile_${params.videoId}.mp4`,
      title: videoDetails.title,
      appendExt: 'mp4',
    })
      .fetch('GET', url)
      .progress((received, total) => {
        console.log('progress', received / total);
      })
      .then(async (resp) => {
        try {
          const dataMusic = {
            path: `${pathDir}/horizon/downloadfile_${params.videoId}.mp4`,
            id: params.videoId,
            title: videoDetails.title,
            channel: videoDetails.ownerChannelName,
            channelId: videoDetails.channelId,
          };
          await setMusics(dataMusic);
          await setDownload(false);
        } catch (error) {
          console.log(error);
          Alert.alert('Ops!', 'Algo não funcionou como esperado');
        }
      });
  }

  useEffect(() => {
    setDetails({});
    async function getDatails() {
      try {
        const response = await api.get(
          `https://www.googleapis.com/youtube/v3/videos?key=AIzaSyDn8cOcNwm6JJWK78KVbaRk663ihWOYqc4&part=snippet&id=${params.videoId}`,
        );
        setDetails(response.data.items[0]);
      } catch (error) {
        console.log(error.message);
      }
    }

    getDatails();
  }, [params]);
  return (
    <View style={{position: 'relative', flex: 1, backgroundColor: '#fff'}}>
      <StatusBar
        barStyle="light-content"
        backgroundColor="transparent"
        translucent
      />
      <HeaderBox>
        <TouchableOpacity
          onPress={() => goBack()}
          disabled={download}
          style={{
            flex: 1,
          }}>
          <Icon size={30} name="arrow-left" color="#fff" />
        </TouchableOpacity>
      </HeaderBox>
      {details.snippet ? (
        <>
          <View
            style={{width: SCREEN_WIDTH, height: 280, position: 'relative'}}>
            {/* <Image
              source={{uri: details.snippet.thumbnails.high.url}}
              style={{flex: 1}}
              resizeMode="cover"
            /> */}
            <YouTube
              videoId={params.videoId ? params.videoId : ''}
              play={true} // control playback of video with true/false
              onError={(e) => console.log('1 error')}
              style={{flex: 1}}
              showinfo={false}
              hidden={true}
              modestbranding={true}
            />
            <View
              style={{
                width: SCREEN_WIDTH,
                height: 280,
                position: 'absolute',
                top: 0,
                zIndex: 8,
                backgroundColor: 'rgba(0,0,0,0.3)',
              }}></View>
          </View>

          <ScrollView style={{flex: 1}}>
            <Container>
              <Title style={{marginBottom: 10}}>{details.snippet.title}</Title>
              <Title style={{color: '#ff5e62', marginBottom: 25}}>
                {details.snippet.channelTitle}
              </Title>
              <Description>{details.snippet.description}</Description>
            </Container>
          </ScrollView>
        </>
      ) : (
        <Title style={{color: '#ff5e62'}}>Musica não encontrada</Title>
      )}

      <Button onPress={() => downloadVideo()} disabled={download}>
        {download ? (
          <ActivityIndicator size="small" color="#fff" size={28} />
        ) : (
          <>
            <Icon
              size={28}
              name="download"
              color="#fff"
              style={{marginHorizontal: 10}}
            />
            <Title style={{color: '#fff'}}>Download</Title>
          </>
        )}
      </Button>
    </View>
  );
};

export default Music;
