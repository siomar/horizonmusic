import React, {useCallback, useEffect, useState} from 'react';
import {View, Text, Image, TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

import {MusicList, TitleList} from './styles';
import {useSongs} from '../../hooks/songs';

const Playlist: React.FC = () => {
  const {getMusics} = useSongs();
  const [songs, setSongs] = useState([]);
  useEffect(() => {
    async function getVideo() {
      const videos = await getMusics();
      setSongs(videos);
    }
    getVideo();
  }, []);

  return (
    <View style={{backgroundColor: '#fff', flex: 1}}>
      <MusicList
        data={songs}
        ListHeaderComponent={
          <View style={{paddingBottom: 24}}>
            <TitleList>Suas musicas</TitleList>
          </View>
        }
        keyExtractor={(item) => item.id.videoId}
        ListEmptyComponent={
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Icon
              name="download"
              size={45}
              color={'#adadad'}
              style={{margin: 15}}
            />
            <Text style={{color: '#adadad'}}>
              Baixe musicas para ouvir offline
            </Text>
          </View>
        }
        style={{
          width: '100%',
          paddingHorizontal: 24,
        }}
        renderItem={(item, index) => (
          <View
            key={index + item.item.id.videoId}
            style={{
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginBottom: 25,
            }}>
            <View
              style={{
                width: 75,
                height: 75,
                borderRadius: 12,
                backgroundColor: '#ff5e62',
                justifyContent: 'center',
                alignItems: 'center',
                marginRight: 20,
              }}>
              <Icon name="music" size={45} color={'#fff'} />
            </View>
            <View
              style={{
                flex: 1,
              }}>
              <Text style={{fontSize: 16, marginBottom: 5, flex: 1}}>
                {item.item.title}
              </Text>
              <Text
                style={{
                  fontSize: 14,
                  marginBottom: 14,
                  flex: 1,
                  color: '#ff5e62',
                }}>
                {item.item.channel}
              </Text>
            </View>
          </View>
        )}
      />
    </View>
  );
};

export default Playlist;
