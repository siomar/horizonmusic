import {FlatList} from 'react-native';
import styled from 'styled-components/native';

export const Container = styled.View`
  width: 100%;
  padding: 60px 24px;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;
