import React from 'react';
import {View, Text} from 'react-native';

import {Container} from './styles';
import categorias from '../../assets/generos.json';
const Intro: React.FC = () => {
  return (
    <Container
      style={{
        flex: 1,
      }}>
      <Text style={{fontSize: 22, marginVertical: 25}}>
        Selecione seus generos
      </Text>
    </Container>
  );
};

export default Intro;
