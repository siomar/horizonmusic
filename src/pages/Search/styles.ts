import {FlatList} from 'react-native';
import styled from 'styled-components/native';

export const HeaderBox = styled.View`
  width: 100%;
  padding: 60px 24px 20px;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const MusicList = styled(FlatList as new () => FlatList)`
  width: 100%;
  padding-left: 24px;
  margin-bottom: 50px;
  margin: 0;
`;

export const TitleList = styled.Text`
  font-family: 'Montserrat-Regular';
  font-weight: 800;
  font-size: 18px;
  color: #070707;
  align-items: center;
  margin-bottom: 20px;
`;
