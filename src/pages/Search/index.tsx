import React, {useCallback, useState} from 'react';
import {View, Text, Image, TextInput, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

import {MusicList, TitleList} from './styles';
import api from '../../services/api';
import {useNavigation} from '@react-navigation/native';

const Search: React.FC = () => {
  const [searchValue, setSearchValue] = useState('');
  const [songSearch, setSongSearch] = useState([]);
  const navigation = useNavigation();

  const handleVideoDetails = useCallback(
    (item) => {
      navigation.navigate('Music', {videoId: item.id});
    },
    [navigation],
  );

  const searchVideo = useCallback(async () => {
    try {
      setSongSearch([]);
      const response = await api.get(
        `https://www.googleapis.com/youtube/v3/search?key=AIzaSyDn8cOcNwm6JJWK78KVbaRk663ihWOYqc4&part=snippet&maxResults=50&type=video&order=viewCount&q=${searchValue}`,
      );
      setSongSearch(response.data.items);
    } catch (error) {
      console.log(error);
    }
  }, [searchValue]);

  return (
    <View style={{backgroundColor: '#fff', flex: 1}}>
      <MusicList
        data={songSearch}
        ListHeaderComponent={
          <View style={{paddingBottom: 24}}>
            <TitleList>Busque por musicas</TitleList>
            <TextInput
              style={{height: 40, borderColor: 'gray', borderBottomWidth: 0.5}}
              onChangeText={(text) => setSearchValue(text)}
              placeholder="Busque aqui"
              value={searchValue}
              onSubmitEditing={() => searchVideo()}
            />
          </View>
        }
        keyExtractor={(item) => item.id.videoId}
        ListEmptyComponent={
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Icon
              name="search"
              size={35}
              color={'#adadad'}
              style={{margin: 15}}
            />
            <Text style={{color: '#adadad'}}>
              Busque por musicas no campo acima
            </Text>
          </View>
        }
        style={{
          width: '100%',
          paddingHorizontal: 24,
        }}
        renderItem={(item, index) => (
          <TouchableOpacity
            onPress={() => handleVideoDetails({id: item.item.id.videoId})}>
            <View
              key={index}
              style={{
                width: '100%',
                paddingTop: 15,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <Image
                source={{uri: item.item.snippet.thumbnails.medium.url}}
                resizeMode="cover"
                style={{
                  width: 120,
                  height: 70,
                  borderRadius: 5,
                  marginRight: 15,
                }}
              />

              <View
                style={{
                  flex: 1,
                  justifyContent: 'space-between',
                }}>
                <Text style={{fontSize: 14, marginBottom: 14, flex: 1}}>
                  {item.item.snippet.title}
                </Text>
                <Text
                  style={{
                    fontSize: 12,
                    marginBottom: 14,
                    flex: 1,
                    color: '#ff5e62',
                  }}>
                  {item.item.snippet.channelTitle}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

export default Search;
