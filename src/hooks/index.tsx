import React from 'react';

import {Play} from './play';
import {Songs} from './songs';

const AppProvider: React.FC = ({children}) => (
  <Songs>
    <Play>{children}</Play>
  </Songs>
);

export default AppProvider;
