import React, {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useState,
} from 'react';
import {Platform, View} from 'react-native';

import ytdl from 'react-native-ytdl';
// import RNFetchBlob from 'react-native-fetch-blob';
// import {checkMultiple, request, PERMISSIONS} from 'react-native-permissions';

// import { Container } from './styles';

// import {checkMultiple, request, PERMISSIONS} from 'react-native-permissions';
import RNFetchBlob from 'rn-fetch-blob';

import AsyncStorage from '@react-native-async-storage/async-storage';

interface ISongsContext {
  setMusics(data: SongState): void;
  getMusics(): SongState[];
  data: SongState[];
}

interface YdtlData {
  url: string;
}

interface SongState {
  path: string;
  id: string;
  title: string;
  channel: string;
  channelId: string;
}

const SongsContextProvider = createContext<ISongsContext>({});

export const Songs: React.FC = ({children}) => {
  const [data, setData] = useState<SongState[]>([]);

  useEffect(() => {
    async function loadStoragedData(): Promise<void> {
      // await AsyncStorage.removeItem('@horizon_songs');
      const musics = await AsyncStorage.getItem('@horizon_musics');

      setData(musics ? JSON.parse(musics) : []);

      console.log('AsyncStorage', data);
    }
    loadStoragedData();
  }, []);

  useEffect(() => {
    async function setAsync() {
      await AsyncStorage.setItem('@horizon_musics', JSON.stringify(data));
    }

    setAsync();
  }, [data]);

  const setMusics = useCallback(
    async (dataMusic: SongState) => {
      try {
        setData((item) => [...data, dataMusic]);
        await AsyncStorage.setItem('@horizon_musics', JSON.stringify(data));
      } catch (error) {
        console.log(error);
      }
    },
    [data],
  );

  const getMusics = useCallback(async (dataMusic: SongState) => {
    const data = await AsyncStorage.getItem('@horizon_musics');
    return JSON.parse(data);
  }, []);

  return (
    <SongsContextProvider.Provider value={{setMusics, getMusics, data: data}}>
      {children}
    </SongsContextProvider.Provider>
  );
};

export function useSongs(): ISongsContext {
  const context = useContext(SongsContextProvider);

  if (!context) {
    throw new Error('Erro ao conectar no provider do player');
  }

  return context;
}
