import React, {createContext, useCallback, useContext, useState} from 'react';
import {View} from 'react-native';

// import ytdl from 'react-native-ytdl';
// import RNFetchBlob from 'react-native-fetch-blob';
// import {checkMultiple, request, PERMISSIONS} from 'react-native-permissions';

// import { Container } from './styles';

interface Iid {
  videoId: string;
  id: string;
}

interface ISongs {
  kind: string;
  etag: string;
  id: Iid;
  snippet: {
    publishedAt: string;
    channelId: string;
    title: string;
    description: string;
    thumbnails: {
      default: {
        url: string;
        width: number;
        height: number;
      };
      url: {
        url: string;
        width: number;
        height: number;
      };
      high: {
        url: string;
        width: number;
        height: number;
      };
    };
    channelTitle: string;
    liveBroadcastContent: string;
    publishTime: string;
  };
}
interface IPlayContext {
  songSeleted: ISongs;
  openPlayer(item: any): void;
  player: boolean;
}

const PlayContextProvider = createContext<IPlayContext>({});

export const Play: React.FC = ({children}) => {
  const [songSeleted, setSongSeleted] = useState<ISongs>({} as ISongs);
  const [open, setOpen] = useState<boolean>(false);
  const openPlayer = useCallback((item: ISongs) => {
    setSongSeleted(item);
  }, []);

  return (
    <PlayContextProvider.Provider
      value={{
        songSeleted,
        openPlayer,
        player: open,
      }}>
      {children}
    </PlayContextProvider.Provider>
  );
};

export function usePlay(): IPlayContext {
  const context = useContext(PlayContextProvider);

  if (!context) {
    throw new Error('Erro ao conectar no provider do player');
  }

  return context;
}
